# Raspberry Burst coin monitor #
This can be used to check your burst wallet balance and the current burst rate periodically. No need of the pass phrase for this, only the numeric account id. I used python language for the programming part. 16x2 Display is connected with the I2C module for the communication between the raspberry. Power supply is 5V 0.7A supply.

This also can monitor BTC rate and Burst balance in USD.

Display changes every 5 seconds between Burst and USD.

Update data every 3 minutes

![1487863548570-11.jpeg](https://bitbucket.org/repo/o84dgL/images/1960889882-1487863548570-11.jpeg)

![1487863557216-22.jpeg](https://bitbucket.org/repo/o84dgL/images/1249505374-1487863557216-22.jpeg)