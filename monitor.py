#!/usr/bin/python

import smbus
import time
import socket
import requests
import urllib
import urllib2
import json
import urllib
import urllib2

I2C_ADDR  = 0x3f # I2C device address
LCD_WIDTH = 16   # Maximum characters per line

LCD_CHR = 1 # Mode - Sending data
LCD_CMD = 0 # Mode - Sending command

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line
LCD_LINE_3 = 0x94 # LCD RAM address for the 3rd line
LCD_LINE_4 = 0xD4 # LCD RAM address for the 4th line

LCD_BACKLIGHT  = 0x08  # On

ENABLE = 0b00000100 # Enable bit

E_PULSE = 0.0005
E_DELAY = 0.0005

bus = smbus.SMBus(1)

def lcd_init():
  lcd_byte(0x33,LCD_CMD) # 110011 Initialise
  lcd_byte(0x32,LCD_CMD) # 110010 Initialise
  lcd_byte(0x06,LCD_CMD) # 000110 Cursor move direction
  lcd_byte(0x0C,LCD_CMD) # 001100 Display On,Cursor Off, Blink Off 
  lcd_byte(0x28,LCD_CMD) # 101000 Data length, number of lines, font size
  lcd_byte(0x01,LCD_CMD) # 000001 Clear display
  time.sleep(E_DELAY)

def lcd_byte(bits, mode):
  bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT
  bits_low = mode | ((bits<<4) & 0xF0) | LCD_BACKLIGHT
  bus.write_byte(I2C_ADDR, bits_high)
  lcd_toggle_enable(bits_high)
  bus.write_byte(I2C_ADDR, bits_low)
  lcd_toggle_enable(bits_low)

def lcd_toggle_enable(bits):
  time.sleep(E_DELAY)
  bus.write_byte(I2C_ADDR, (bits | ENABLE))
  time.sleep(E_PULSE)
  bus.write_byte(I2C_ADDR,(bits & ~ENABLE))
  time.sleep(E_DELAY)

def lcd_string(message,line):
  message = message.ljust(LCD_WIDTH," ")
  lcd_byte(line, LCD_CMD)

  for i in range(LCD_WIDTH):
    lcd_byte(ord(message[i]),LCD_CHR)

def connect(url,values):
	data = urllib.urlencode(values)
        req = urllib2.Request(url, data)
	try:
        	response = urllib2.urlopen(req)
        	the_page = response.read()
        	final_data = json.loads(the_page)
		return final_data
	except:
		return 0	

def main():
  lcd_init()
  lcd_string("Connecting......",LCD_LINE_1)

  while True:

	url = 'https://wallet.burst-team.us:8125/burst'
	values = {'requestType' : 'getAccount',
	'account' : 'your numeric account id'}
	data = connect(url,values)
	if(data == 0):
		lcd_string("Error Network 1",LCD_LINE_1)
		time.sleep(10)
		continue
#	balance=float(data['guaranteedBalanceNQT'])/100000000
	balance=float(data['unconfirmedBalanceNQT'])/100000000
	
#========= Get Burst Rate =====================	

	url = 'https://poloniex.com/public?command=returnTicker'
	values = {}
	data = connect(url,values)
	if(data == 0):
                lcd_string("Error Network 2",LCD_LINE_1)
                time.sleep(10)
                continue
	burst_btc_rate=float(data['BTC_BURST']['highestBid'])
	
	url='https://api.coindesk.com/v1/bpi/currentprice.json'
	values = {}
	data = connect(url,values)
        if(data == 0):
                lcd_string("Error Network 3",LCD_LINE_1)
                time.sleep(10)
                continue
	usd_btc_rate = float(data["bpi"]["USD"]["rate"].replace(',', '')) 
	burst_usd_bal = balance * burst_btc_rate * usd_btc_rate
	
	#display on lcd	
	for x in range(0, 18):
		lcd_string("Bal:"+"{0:.0f}".format(balance)+" BURST",LCD_LINE_1)
    		lcd_string("Rate:"+"{0:.2f}".format(burst_btc_rate*1000000)+" uBTC",LCD_LINE_2)
		time.sleep(5)
		lcd_string("Bal:"+"{0:.3f}".format(burst_usd_bal)+" USD",LCD_LINE_1)
		lcd_string("BTC:"+"{0:.0f}".format(usd_btc_rate)+" USD",LCD_LINE_2)
                time.sleep(5)



if __name__ == '__main__':

  try:
    main()
  except KeyboardInterrupt:
    pass
  finally:
    lcd_byte(0x01, LCD_CMD)

